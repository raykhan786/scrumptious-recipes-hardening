from django import template
register = template.Library()


def resize_to(ingredient, target):
    num_of_servings = ingredient.recipe.servings
    amount = ingredient.amount
    if num_of_servings is not None and target is not None:
        ratio = int(target)/int(num_of_servings)
        new_amount = ratio * int(amount)
        return new_amount
    else:
        return amount


register.filter(resize_to)
